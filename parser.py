# coding: utf8
import sys
import requests
import re
import os
import logging
import googlesearch
import multiprocessing
from joblib import Parallel, delayed

logging.basicConfig(filename="parser.log",
                    filemode='a',
                    format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                    datefmt='%H:%M:%S',
                    level=logging.DEBUG)

def google(query):
    #headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'}
    #url = 'https://www.duckduckgo.com/?q=war3.replays.net: ' + query
    #res = requests.get(url, headers=headers)
    #return res.text

    return list(googlesearch.search(
        'war3.replays.net: ' + query, 
        user_agent='Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'
    ))[0]
    

class Parser:
    # lp = List Page
    def __init__(self, lp=1, lp_start=1, lp_end=2):
        self.lp = lp
        self.lp_start = lp_start
        self.lp_end = lp_end

    def parse_lp(self):
        page_number = str(self.lp)

        r = requests.get("http://war3.replays.net/index.php?/list/3/0/" + page_number)
        #print(r.text)

        # open(page_number, 'wb').write(r.content)

        # Find all articles
        x = re.findall('<div class="news_item">.*?<\/div>', str(r.text))

        logging.info("List Page: " + page_number)
        for val in x:
            # link in html
            link1, article_title = (re.findall('<a target="_blank" href="([^"]*?)">(.*?)</a>', val))[0]
            #exec('print(b\''+ article_title + '\'.decode(\'utf-8\'))')

            print(article_title)
            # day on the left
            day = (re.findall('class="news_date" data-date="(\d+)"', val))[0]
            # older links need day in url
            link2 = re.sub('page\/(\d{6})', 'page/\g<1>' + day, link1)
            # create filename date-articleid
            filename = str((re.findall("page\/(\d{8}\/\d+)", link2))[0])
            filename = re.sub('\/', '-', filename)
            dirname = filename
            filename = filename + ".html"

            # get month and year
            year, month = (re.findall('page\/(\d{4})(\d{2})', link1))[0]

            # article link
            logging.info("Try : " + link1)
            r = requests.get(link1)
            if r.status_code == 200:
                logging.info("----Download to %s ----" % (filename))
            if r.status_code == 404:
                # if 404 then try link2 with added day
                logging.info("----Status 404----")
                logging.info("----try : " + link2)
                r = requests.get(link2)
                if r.status_code == 404:
                    logging.info("!!!! Status code 404 -> list page %s, link1 %s, link2 %s!!!!" % (page_number, link1, link2))
                    logging.info("!!!! Try google search: war3.replays.net: %s" % (article_title))
                    print(google(article_title))
                    #first_result = list(googlesearch.search('war3.replays.net: "' + article_title + '"'))[0] 
                    #if len(first_result) == 0:
                    #    first_result = list(googlesearch.search('war3.replays.net: ' + article_title))[0]
                    if re.search("war3\.replays\.net\/news\/page\/\d{6,8}/\d+\.html", first_result):
                        r = requests.get(first_result)
                        if r.status_code == 200:
                            logging.info("----Download to %s ----" % (filename))
                        if r.status_code == 404:
                            logging.error("Cannot")
                    else:
                        logging.error("!!!! couldn't find google match for %s" % (article_title))
                        next
                if r.status_code == 200:
                    logging.info("----Download to %s ----" % (filename))

            if not os.path.exists(year):
                os.mkdir(year)

            if not os.path.exists(year + "/" + month):
                os.mkdir(year + "/" + month)

            f = open("/".join([year, month, filename]), 'wb').write(r.content)

            if not os.path.exists("/".join([year, month, dirname])):
                os.mkdir("/".join([year, month, dirname]))

            # get imgs

            imgs = (re.findall('<img[^>]+?src="([^"]+)"', str(r.content)))
            for img in imgs:
                try:
                    # 404 without header
                    rr = requests.get(img, headers={'Referer': 'http://war3.replays.net/'})
                    if rr.status_code == 200:
                        imgdir, imgname = (os.path.split(img))
                        imgdir = re.sub('https?:\/\/', '', imgdir)
                        os.makedirs("/".join([year, month, dirname, imgdir]), exist_ok=True)
                        if (os.path.exists("/".join([year, month, dirname, imgdir, imgname]))):
                            logging.info("------img: %s exists" % (img))
                        else:
                            open("/".join([year, month, dirname, imgdir, imgname]), 'wb').write(rr.content)
                            logging.info("------img: %s downloaded" % (img))
                        logging.info("------change img src")
                        f = open("/".join([year, month, filename]), "rb")
                        s = f.read()
                        f.close()
                        s = s.replace((bytes(img, encoding='utf8')),
                                      bytes("/".join([dirname, imgdir, imgname]), encoding='utf8'))
                        open("/".join([year, month, filename]), 'wb').write(s)
                        logging.info("------change img src DONE")
                except:
                    logging.error("------couldn't download %s : %s" % (img, sys.exc_info()[0]))

    def parse_range(self):
        for i in range(self.lp_start, self.lp_end + 1):
            self.lp = i
            self.parse_lp()


if __name__ == '__main__':
    par = Parser(
        lp=5,
        lp_start=1,
        lp_end=2
        # lp_end=835
    )

    par.parse_lp()
    # par.parse_range()
    # Parallel(n_jobs=12)(delayed(Parser(lp=1, lp_start=1, lp_end=2).parse_lp(lp=i)) for i in range(1, 10))
    # with multiprocessing.Pool(processes=6) as pool:
    #    [pool.apply_async(Parser().parse_lp(i), ()) for i in range(10)]
